package pt.unl.fct.di.apdc.almadaconsigo.resources;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pt.unl.fct.di.apdc.almadaconsigo.util.AuthToken;
import pt.unl.fct.di.apdc.almadaconsigo.util.LoginData;
import pt.unl.fct.di.apdc.almadaconsigo.util.OperationData;

@Path("/logout")
public class LogoutResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new GsonBuilder().setPrettyPrinting().create();
	
	public LogoutResource() { } //Nothing to be done here...

	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLogout(OperationData data) {
		LOG.fine("Attempt to logout user: " + data.email);
		
	
		Transaction txn = datastore.beginTransaction();
		
		try {
			
			Key userKey = KeyFactory.createKey("User", data.email);
			Entity user = datastore.get(userKey);
			
			Query ctrQuery = new Query("UserToken").setAncestor(userKey);
			Entity token = datastore.prepare(ctrQuery).asSingleEntity();
			if(token.getKey() == null){
				LOG.warning("User not logged in: " + data.email);
				return Response.status(Status.NOT_FOUND).build();
			} else {
				datastore.delete(txn, token.getKey());
				txn.commit();
				LOG.info("User '" + data.email + "' logged out sucessfully.");
				return Response.ok("{}").build();
			}
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed logout attempt for username: " + data.email);
			return Response.status(Status.FORBIDDEN).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}
	}
}
