package pt.unl.fct.di.apdc.almadaconsigo.resources;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.almadaconsigo.util.ErrorJSON;
import pt.unl.fct.di.apdc.almadaconsigo.util.LoginData;
import pt.unl.fct.di.apdc.almadaconsigo.util.RegisterData;

@Path("/register")
public class RegisterResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(RegisterResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new Gson();

	public RegisterResource() {
	} // Nothing to be done here...


	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doRegistration(RegisterData data) {

		if (!data.validRegistration()) {
			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}

		Transaction txn = datastore.beginTransaction();

		Query ctrQuery = new Query("User");

		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
		for (Entity ent : results) {
			if (ent.getProperty("user_cc").equals(data.cc) || ent.getProperty("user_nif").equals(data.nif)
					|| ent.getKey().getName().equals(data.email)) {
				txn.rollback();
				return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorJSON("User already exists."))).build();
			}
		}
		Entity user = new Entity("User", data.email);
		user.setProperty("user_name", data.name);
		user.setProperty("user_pwd", DigestUtils.sha512Hex(data.password));
		user.setProperty("user_telephone", data.telephone);
		user.setProperty("user_mobile", data.mobile);
		user.setProperty("user_street", data.street);
		user.setProperty("user_compl", data.compl);
		user.setProperty("user_city", data.city);
		user.setProperty("user_cp", data.cp);
		user.setIndexedProperty("user_nif", data.nif);
		user.setIndexedProperty("user_cc", data.cc);
		user.setUnindexedProperty("user_creation_time", new Date());
		datastore.put(txn, user);
		LOG.info("User registered " + data.email);
		txn.commit();
		if (txn.isActive()) {
			txn.rollback();
		}
		return Response.ok("{}").build();
	}
}
