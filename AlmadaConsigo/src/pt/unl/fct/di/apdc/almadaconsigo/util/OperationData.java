package pt.unl.fct.di.apdc.almadaconsigo.util;

public class OperationData {

	public String email;
	public String id;
	
	public OperationData(){
		
	}
	
	public OperationData(String email, String id){
		this.email = email;
		this.id = id;
	}
}
