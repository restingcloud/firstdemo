package pt.unl.fct.di.apdc.almadaconsigo.util;

public class RegisterData {

	public String name;
	public String email;
	public String telephone;	
	public String mobile;
	public String street;
	public String compl;
	public String city;
	public String cp;
	public String nif;
	public String cc;
	public String password;
	public String confirmation;
	
	public RegisterData() {
		
	}
	
	public RegisterData(String name, String email, String telephone, String mobile,
			String street, String compl, String city, String cp, String nif, String cc, String password, String confirmation) {
		
			this.name = name;
			this.email = email;
			this.telephone = telephone;
			this.mobile = mobile;
			this.street = street;
			this.compl = compl;
			this.city = city;
			this.cp = cp;
			this.nif = nif;
			this.cc = cc;
			this.password = password;
			this.confirmation = confirmation;
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
	public boolean validRegistration() {
		return validField(name) &&
			   validField(email) &&
			   validField(telephone) &&
			   validField(street) &&
			   validField(nif) &&
			   validField(cc) &&
			   validField(password) &&
			   validField(confirmation) &&
			   password.equals(confirmation) &&
			   email.contains("@");		
	}
	
}
