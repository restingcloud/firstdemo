package pt.unl.fct.di.apdc.almadaconsigo.resources;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;

@Path("/utils")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class DeleteOldTokensResource {
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new Gson();

	public DeleteOldTokensResource() {
	} // nothing to be done here

	@GET
	@Path("/deleteOldTokens")
	public Response executeComputeTask() {
		LOG.fine("Starting to delete old tokens");
		
		Transaction txn = null;
		try {
			Query ctrQuery = new Query("UserToken");
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
			for(Entity i : results)
				if(((long)i.getProperty("expiration_date")) < System.currentTimeMillis()){
					txn = datastore.beginTransaction();
					datastore.delete(i.getKey());
					txn.commit();
					if (txn.isActive())
						txn.rollback();
				}
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
			Thread.sleep(60 * 60 * 1000); // 1 hour...
		} catch (Exception e) {
			LOG.logp(Level.SEVERE, this.getClass().getCanonicalName(), "executeComputeTask", "An exception has ocurred",
					e);
			return Response.serverError().build();
		} // Simulates 60s execution
		return Response.ok().build();
	}
	
	
}
